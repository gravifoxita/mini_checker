# Create by gravifoxita #

# Import necessary libraries
import OpenSSL

# Specify the file name and path of the encrypted file
file_path = '/path/to/encrypted_file'

# Read the encrypted file and decode it
with open(file_path, 'rb') as f:
    encrypted_data = f.read()

# Get the cipher algorithm used for encryption
cipher_algorithm = OpenSSL.crypto.get_cipher_name(encrypted_data<:16>)

# Print the cipher algorithm used
print("The cipher algorithm used for encryption is:", cipher_algorithm)